FROM ruby:2.6-alpine

RUN apk --no-cache add curl bash openssl sudo git py-pip groff
RUN pip install awscli

ARG HELM_VERSION
RUN curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get -o /tmp/get && \
    if [ -n "${HELM_VERSION}" ]; then \
        bash /tmp/get --version ${HELM_VERSION}; \
    else \
        bash /tmp/get; \
    fi && \
    helm version --client

